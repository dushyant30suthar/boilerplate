package com.matrixhive.boilerplate.framework;

import android.app.Application;

import com.matrixhive.boilerplate.domainlayer.datasources.database.DatabaseModule;
import com.matrixhive.boilerplate.domainlayer.datasources.network.NetworkModule;
import com.matrixhive.boilerplate.framework.dicomponents.DaggerDataLayerComponent;
import com.matrixhive.boilerplate.framework.dicomponents.DaggerViewModelComponent;
import com.matrixhive.boilerplate.framework.dicomponents.DataLayerComponent;
import com.matrixhive.boilerplate.framework.dicomponents.ViewModelComponent;
import com.matrixhive.boilerplate.viewmodellayer.ViewModelModule;

public class BoilerplateApplication extends Application {

    protected static DataLayerComponent dataLayerComponent;

    private static ViewModelComponent viewModelComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setDataLayerComponent();
        setViewModelComponent();
    }

    protected void setDataLayerComponent() {
        dataLayerComponent = DaggerDataLayerComponent.builder()
                .networkModule(new NetworkModule(this))
                .databaseModule(new DatabaseModule(this))
                .build();
    }

    private void setViewModelComponent() {
        viewModelComponent = DaggerViewModelComponent.builder()
                .viewModelModule(new ViewModelModule())
                .build();
    }

    public static DataLayerComponent getDataLayerComponent() {
        return dataLayerComponent;
    }

    public static ViewModelComponent getViewModelComponent() {
        return viewModelComponent;
    }
}
