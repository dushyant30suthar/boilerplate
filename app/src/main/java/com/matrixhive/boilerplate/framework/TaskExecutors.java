package com.matrixhive.boilerplate.framework;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class TaskExecutors {
    private Executor diskOperationThread;
    private Executor networkOperationThread;
    private Executor mainThread;

    /*
     * methods to dispatch tasks on demanded thread.*/

    @Inject
    public TaskExecutors() {
        this.diskOperationThread = Executors.newSingleThreadExecutor();
        this.networkOperationThread = Executors.newFixedThreadPool(3);
        this.mainThread = new MainThreadExecutor();
    }

    public Executor getDiskOperationThread() {
        return diskOperationThread;
    }

    public Executor getNetworkOperationThread() {
        return networkOperationThread;
    }

    public Executor getMainThread() {
        return mainThread;
    }

    private class MainThreadExecutor implements Executor {
        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            mainThreadHandler.post(command);
        }
    }
}