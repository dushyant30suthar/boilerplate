package com.matrixhive.boilerplate.framework.dicomponents;

import com.matrixhive.boilerplate.domainlayer.datasources.database.DatabaseModule;
import com.matrixhive.boilerplate.domainlayer.datasources.network.NetworkModule;
import com.matrixhive.boilerplate.domainlayer.repositories.MovieRepository;

import javax.inject.Singleton;

import dagger.Component;

@Component(
        modules = {NetworkModule.class, DatabaseModule.class})
@Singleton
public interface DataLayerComponent {
    void doInjection(MovieRepository movieRepository);
}