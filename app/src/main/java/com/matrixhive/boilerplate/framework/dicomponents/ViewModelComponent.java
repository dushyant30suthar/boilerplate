package com.matrixhive.boilerplate.framework.dicomponents;

import com.matrixhive.boilerplate.clientlayer.presentations.movie.fragments.MovieDetailsFragment;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.fragments.MovieListFragment;
import com.matrixhive.boilerplate.viewmodellayer.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(
        modules = {ViewModelModule.class})
@Singleton
public interface ViewModelComponent {
    /*
     * This component would provide movieViewModel module
     * through which we can get all the viewModels at their desired place.*/
    void doInjection(MovieListFragment movieListFragment);

    void doInjection(MovieDetailsFragment movieDetailsFragment);
}
