package com.matrixhive.boilerplate.clientlayer.presentations.movie.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.MoviePresenter;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.MovieViewModel;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.ViewController;
import com.matrixhive.boilerplate.databinding.FragmentMovieDetailsBinding;
import com.matrixhive.boilerplate.framework.BoilerplateApplication;
import com.matrixhive.boilerplate.viewmodellayer.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

public class MovieDetailsFragment extends Fragment implements ViewController {

    @Inject
    ViewModelFactory viewModelFactory;

    private FragmentMovieDetailsBinding fragmentMovieDetailsBinding;

    private MovieViewModel movieViewModel;
    private MoviePresenter moviePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BoilerplateApplication.getViewModelComponent().doInjection(this);
        movieViewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMovieDetailsBinding = FragmentMovieDetailsBinding.inflate(inflater, container, false);
        return getView() != null ? getView() : fragmentMovieDetailsBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        moviePresenter = new MoviePresenter(movieViewModel, this, MoviePresenter.PresenterType.DETAILS);
        fragmentMovieDetailsBinding.setMoviePresenter(moviePresenter);
        fragmentMovieDetailsBinding.setLifecycleOwner(getLifeCycleOwner());
    }

    @BindingAdapter({"imageSource"})
    public static void setImage(ImageView view, String url) {
        Glide.with(view.getContext()).load(url).into(view);
    }

    @Override
    public void onSucceed() {
    }

    @Override
    public void onLoadingOccurred() {

    }

    @Override
    public void onErrorOccurred(String message) {
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle bundle = getArguments();
        if (bundle != null)
            moviePresenter.getMovieIdLiveData().setValue(bundle.getString("movie_id"));
    }

    @Override
    public LifecycleOwner getLifeCycleOwner() {
        return getViewLifecycleOwner();
    }

}
