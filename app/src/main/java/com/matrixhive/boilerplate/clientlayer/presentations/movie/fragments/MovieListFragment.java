package com.matrixhive.boilerplate.clientlayer.presentations.movie.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.matrixhive.boilerplate.R;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.MoviePresenter;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.MovieViewModel;
import com.matrixhive.boilerplate.clientlayer.presentations.movie.ViewController;
import com.matrixhive.boilerplate.databinding.FragmentMovieListBinding;
import com.matrixhive.boilerplate.databinding.MovieItemBinding;
import com.matrixhive.boilerplate.domainlayer.models.Movie;
import com.matrixhive.boilerplate.framework.BoilerplateApplication;
import com.matrixhive.boilerplate.viewmodellayer.ViewModelFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

public class MovieListFragment extends Fragment implements ViewController {

    @Inject
    ViewModelFactory viewModelFactory;
    private FragmentMovieListBinding fragmentMovieListBinding;
    private final String RECYCLER_VIEW_STATE_KEY = "RECYCLER_VIEW_STATE_KEY";
    private MovieViewModel movieViewModel;
    private Bundle savedInstanceState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BoilerplateApplication.getViewModelComponent().doInjection(this);
        movieViewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMovieListBinding = FragmentMovieListBinding.inflate(inflater, container, false);
        return getView() != null ? getView() : fragmentMovieListBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setUpInitialThings(view);
        MoviePresenter moviePresenter = new MoviePresenter(movieViewModel, this, MoviePresenter.PresenterType.LIST);
        fragmentMovieListBinding.setMoviePresenter(moviePresenter);
        fragmentMovieListBinding.setLifecycleOwner(getLifeCycleOwner());
    }

    private void setUpInitialThings(View view) {
        MovieListAdapter movieListAdapter = new MovieListAdapter() {
            @Override
            public void onMovieItemClicked(String id) {
                Bundle bundle = new Bundle();
                bundle.putString("movie_id", id);
                Navigation.findNavController(view).navigate(R.id.fragment_movie_details, bundle);
            }
        };
        fragmentMovieListBinding.movieListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentMovieListBinding.movieListRecyclerView.setAdapter(movieListAdapter);
        StartSnapHelper snapHelper = new StartSnapHelper();
        snapHelper.attachToRecyclerView(fragmentMovieListBinding.movieListRecyclerView);
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle bundle = new Bundle();
        bundle.putParcelable(RECYCLER_VIEW_STATE_KEY, Objects.requireNonNull(fragmentMovieListBinding.movieListRecyclerView.getLayoutManager()).onSaveInstanceState());
        setArguments(bundle);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(RECYCLER_VIEW_STATE_KEY, Objects.requireNonNull(fragmentMovieListBinding.movieListRecyclerView.getLayoutManager()).onSaveInstanceState());
    }

    @BindingAdapter("list")
    public static void setList(RecyclerView recyclerView, List<Movie> movieList) {
        ((MovieListAdapter) Objects.requireNonNull(recyclerView.getAdapter())).setMovieList(movieList);
    }

    @Override
    public void onSucceed() {
        savedInstanceState = getArguments();
        if (savedInstanceState != null) {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(RECYCLER_VIEW_STATE_KEY);
            Objects.requireNonNull(fragmentMovieListBinding.movieListRecyclerView.getLayoutManager()).onRestoreInstanceState(savedRecyclerLayoutState);
        }
    }

    @Override
    public void onErrorOccurred(String message) {
    }

    @Override
    public void onLoadingOccurred() {

    }

    @Override
    public LifecycleOwner getLifeCycleOwner() {
        return getViewLifecycleOwner();
    }

    public abstract class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieListViewHolder> {

        List<Movie> movieList;

        MovieListAdapter() {
            this.movieList = new ArrayList<>();
        }

        void setMovieList(List<Movie> movieList) {
            this.movieList = movieList;
            notifyDataSetChanged();
        }

        abstract void onMovieItemClicked(String imdbId);

        @NonNull
        @Override
        public MovieListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            MovieItemBinding binding = MovieItemBinding.inflate(getLayoutInflater(), parent, false);
            return new MovieListViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull MovieListViewHolder holder, int position) {
            Movie movie = movieList.get(position);
            holder.movieItemBinding.getRoot().setOnClickListener(v -> onMovieItemClicked(movie.getImdbID()));
            holder.bind(movie);
        }

        @Override
        public int getItemCount() {
            if (movieList != null) {
                return movieList.size();
            } else {
                return 0;
            }
        }


        class MovieListViewHolder extends RecyclerView.ViewHolder {

            private final MovieItemBinding movieItemBinding;

            MovieListViewHolder(@NonNull MovieItemBinding movieItemBinding) {
                super(movieItemBinding.getRoot());
                this.movieItemBinding = movieItemBinding;
            }

            void bind(Movie movie) {
                movieItemBinding.setMovie(movie);
                movieItemBinding.executePendingBindings();
            }
        }
    }

    class StartSnapHelper extends LinearSnapHelper {

        private OrientationHelper mVerticalHelper, mHorizontalHelper;

        StartSnapHelper() {

        }

        @Override
        public void attachToRecyclerView(@Nullable RecyclerView recyclerView)
                throws IllegalStateException {
            super.attachToRecyclerView(recyclerView);
        }

        @Override
        public int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager,
                                                  @NonNull View targetView) {
            int[] out = new int[2];

            if (layoutManager.canScrollHorizontally()) {
                out[0] = distanceToStart(targetView, getHorizontalHelper(layoutManager));
            } else {
                out[0] = 0;
            }

            if (layoutManager.canScrollVertically()) {
                out[1] = distanceToStart(targetView, getVerticalHelper(layoutManager));
            } else {
                out[1] = 0;
            }
            return out;
        }

        @Override
        public View findSnapView(RecyclerView.LayoutManager layoutManager) {

            if (layoutManager instanceof LinearLayoutManager) {

                if (layoutManager.canScrollHorizontally()) {
                    return getStartView(layoutManager, getHorizontalHelper(layoutManager));
                } else {
                    return getStartView(layoutManager, getVerticalHelper(layoutManager));
                }
            }

            return super.findSnapView(layoutManager);
        }

        private int distanceToStart(View targetView, OrientationHelper helper) {
            return helper.getDecoratedStart(targetView) - helper.getStartAfterPadding();
        }

        private View getStartView(RecyclerView.LayoutManager layoutManager,
                                  OrientationHelper helper) {

            if (layoutManager instanceof LinearLayoutManager) {
                int firstChild = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

                boolean isLastItem = ((LinearLayoutManager) layoutManager)
                        .findLastCompletelyVisibleItemPosition()
                        == layoutManager.getItemCount() - 1;

                if (firstChild == RecyclerView.NO_POSITION || isLastItem) {
                    return null;
                }

                View child = layoutManager.findViewByPosition(firstChild);

                if (helper.getDecoratedEnd(child) >= helper.getDecoratedMeasurement(child) / 2
                        && helper.getDecoratedEnd(child) > 0) {
                    return child;
                } else {
                    if (((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition()
                            == layoutManager.getItemCount() - 1) {
                        return null;
                    } else {
                        return layoutManager.findViewByPosition(firstChild + 1);
                    }
                }
            }

            return super.findSnapView(layoutManager);
        }

        private OrientationHelper getVerticalHelper(RecyclerView.LayoutManager layoutManager) {
            if (mVerticalHelper == null) {
                mVerticalHelper = OrientationHelper.createVerticalHelper(layoutManager);
            }
            return mVerticalHelper;
        }

        private OrientationHelper getHorizontalHelper(RecyclerView.LayoutManager layoutManager) {
            if (mHorizontalHelper == null) {
                mHorizontalHelper = OrientationHelper.createHorizontalHelper(layoutManager);
            }
            return mHorizontalHelper;
        }
    }
}
