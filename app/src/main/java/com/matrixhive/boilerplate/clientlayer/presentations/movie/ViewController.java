package com.matrixhive.boilerplate.clientlayer.presentations.movie;

import androidx.lifecycle.LifecycleOwner;

public interface ViewController {
    LifecycleOwner getLifeCycleOwner();
    void onErrorOccurred(String message);

    void onSucceed();

    void onLoadingOccurred();
}
