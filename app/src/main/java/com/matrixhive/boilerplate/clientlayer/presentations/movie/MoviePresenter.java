package com.matrixhive.boilerplate.clientlayer.presentations.movie;

import com.matrixhive.boilerplate.domainlayer.DataRequest;
import com.matrixhive.boilerplate.domainlayer.models.Movie;

import java.util.List;

import androidx.lifecycle.MutableLiveData;

public class MoviePresenter {

    private PresenterType presenterType;
    private ViewController viewController;


    private MutableLiveData<Boolean> isLoading;

    /*
     * There are the values which are listened by viewModel to do operations.*/

    /*
     * MovieList is bound to this value.*/
    private MutableLiveData<String> movieSearchStringLiveData;

    /*
     * MovieDetails is bound to this value.*/
    private MutableLiveData<String> movieIdLiveData;
    /*
     * These are the values which are listened by UI to render changes.*/

    /*
     * MovieListFragment bound to this value.
     * */
    private MutableLiveData<List<Movie>> movieList;
    /**
     * MovieDetailsFragment bound to this value.
     */
    private MutableLiveData<Movie> movie;


    public MoviePresenter(MovieViewModel movieViewModel, ViewController viewController, PresenterType presenterType) {
        this.viewController = viewController;
        this.presenterType = presenterType;
        isLoading = new MutableLiveData<>();
        if (this.presenterType == PresenterType.LIST) {
            movieList = new MutableLiveData<>();
            movieSearchStringLiveData = movieViewModel.getObservableMovieSearchString();
            movieViewModel.getObservableMovieList().observe(viewController.getLifeCycleOwner(), this::consumeResponse);
        } else {
            movie = new MutableLiveData<>();
            movieIdLiveData = movieViewModel.getObservableMovieIdString();
            movieViewModel.getObservableMovie().observe(viewController.getLifeCycleOwner(), this::consumeResponse);
        }
    }


    private void consumeResponse(
            DataRequest dataRequest) {
        switch (dataRequest.getCurrentState()) {
            case LOADING: {
                /*
                 * If network operation is going on then we will get the data from
                 * database while the operation is being performed.*/
                isLoading.setValue(true);
                if (presenterType == PresenterType.LIST) {
                    movieList.setValue((List<Movie>) dataRequest.getData());
                } else {
                    movie.setValue((Movie) dataRequest.getData());
                }
                viewController.onLoadingOccurred();
            }
            break;
            case SUCCEED: {

                isLoading.setValue(false);
                /*
                 * Retrieve whatever data you expect from here with just one object.*/
                if (presenterType == PresenterType.LIST) {
                    movieList.setValue((List<Movie>) dataRequest.getData());
                } else {
                    movie.setValue((Movie) dataRequest.getData());
                }
                viewController.onSucceed();
            }
            break;
            case FAILED: {
                isLoading.setValue(false);
                viewController.onErrorOccurred("Error");
            }
            break;
        }
    }


    public MutableLiveData<String> getMovieSearchStringLiveData() {
        return movieSearchStringLiveData;
    }

    public MutableLiveData<String> getMovieIdLiveData() {
        return movieIdLiveData;
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<List<Movie>> getMovieList() {
        return movieList;
    }

    public MutableLiveData<Movie> getMovie() {
        return movie;
    }

    public enum PresenterType {
        LIST,
        DETAILS
    }

}
