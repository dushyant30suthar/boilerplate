package com.matrixhive.boilerplate.clientlayer.presentations.movie;

import android.os.Bundle;

import com.matrixhive.boilerplate.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MovieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
