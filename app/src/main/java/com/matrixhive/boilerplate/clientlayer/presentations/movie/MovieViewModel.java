package com.matrixhive.boilerplate.clientlayer.presentations.movie;

import com.matrixhive.boilerplate.domainlayer.DataRequest;
import com.matrixhive.boilerplate.domainlayer.models.Movie;
import com.matrixhive.boilerplate.domainlayer.repositories.MovieRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

public class MovieViewModel extends ViewModel {

    /*
    * In ViewModel we bind Model to Database because we want our app to have single source of
    * information. So basically, we fetch data and store it to database and then update our ui
    * with the data we have in our database.*/
    @Inject
     MovieRepository movieRepository;

    /*
     * Whenever value in both of these variable gets change, it will trigger loadMovie and loadMovieList method respectively
     * in Repository, Which would eventually grab data and update the ui.*/

    private MutableLiveData<String> movieIdLiveData;

    private MutableLiveData<String> movieSearchStringLiveData;

    public MovieViewModel(MovieRepository movieRepository) {
        this.movieRepository=movieRepository;
        movieIdLiveData = new MutableLiveData<>();
        movieSearchStringLiveData=new MutableLiveData<>();
    }

    /*
     * We would get data in ui from here.*/
    LiveData<DataRequest<List<Movie>>> getObservableMovieList()
    {
        /*
         * Now we got a situation, to observe a live data, observer must be a lifecycle owner.
         * ViewModel is not a lifecycle owner because it doesn't have any lifecycle which a live data needs.
         *
         * Methods that we have in Transformation class make the original lifecycle mechanism available here.
         * Thus, we are able to observe changes at this place and perform operations on that and
         * so our ui capable of having the updated data. */
        return Transformations.switchMap(movieSearchStringLiveData,newSearchString -> movieRepository.loadMovieList(newSearchString,false));
    }

    LiveData<DataRequest<Movie>> getObservableMovie()
    {
        return Transformations.switchMap(movieIdLiveData, newMovieId -> movieRepository.loadMovie(newMovieId));
    }

    MutableLiveData<String> getObservableMovieSearchString()
    {
        /*
         * Making a change in value triggers request.*/
        return movieSearchStringLiveData;
    }

    MutableLiveData<String> getObservableMovieIdString() {
        return movieIdLiveData;
    }

}
