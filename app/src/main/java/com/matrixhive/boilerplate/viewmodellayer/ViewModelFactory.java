package com.matrixhive.boilerplate.viewmodellayer;


import com.matrixhive.boilerplate.clientlayer.presentations.movie.MovieViewModel;
import com.matrixhive.boilerplate.domainlayer.repositories.MovieRepository;

import javax.inject.Inject;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.annotations.NonNull;

public class ViewModelFactory
        implements
        ViewModelProvider.Factory {

    private MovieRepository repository;

    /*
     * This app had only one view model. So, we just passed repository here.
     * But in production app we would have many repositories for many view models.
     *
     * Therefore we must provide repository from other way.*/

    @Inject
    public ViewModelFactory(MovieRepository repository) {
        this.repository = repository;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(
            @NonNull Class<T> modelClass) {
        /*
         * Here we can create various view Models.*/
        if (modelClass.isAssignableFrom(MovieViewModel.class)) {
            return (T) new MovieViewModel(repository);
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}