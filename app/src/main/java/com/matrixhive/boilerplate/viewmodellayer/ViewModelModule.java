package com.matrixhive.boilerplate.viewmodellayer;

import com.matrixhive.boilerplate.domainlayer.repositories.MovieRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelModule {

    /*
     * Here we would provide all the viewModels.*/

    @Provides
    @Singleton
    ViewModelFactory getViewModelFactory()
    {
        return new ViewModelFactory(new MovieRepository());
    }

    /*
     * Other viewModels goes here.*/
}
