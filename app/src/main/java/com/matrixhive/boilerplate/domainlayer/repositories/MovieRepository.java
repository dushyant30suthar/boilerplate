package com.matrixhive.boilerplate.domainlayer.repositories;

import android.annotation.SuppressLint;

import com.matrixhive.boilerplate.domainlayer.DataRequest;
import com.matrixhive.boilerplate.domainlayer.datasources.database.DatabaseModule;
import com.matrixhive.boilerplate.domainlayer.datasources.network.ApiUrls;
import com.matrixhive.boilerplate.domainlayer.datasources.network.NetworkModule;
import com.matrixhive.boilerplate.domainlayer.datasources.network.NetworkOperationObserver;
import com.matrixhive.boilerplate.domainlayer.managers.DataManager;
import com.matrixhive.boilerplate.domainlayer.managers.FetchLimiter;
import com.matrixhive.boilerplate.domainlayer.models.Movie;
import com.matrixhive.boilerplate.domainlayer.models.Response;
import com.matrixhive.boilerplate.framework.BoilerplateApplication;
import com.matrixhive.boilerplate.framework.TaskExecutors;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.schedulers.Schedulers;

/*
 * This class acts as a mediator between database, network and any frontend component.*/
@Singleton
public class MovieRepository {

    @Inject
    TaskExecutors taskExecutors;
    @Inject
    NetworkModule.MovieListApi movieListApi;
    @Inject
    DatabaseModule.MovieDatabase movieDatabase;

    private FetchLimiter movieListFetchLimiter;

    @Inject
    public MovieRepository() {
        BoilerplateApplication.getDataLayerComponent().doInjection(this);
        movieListFetchLimiter = new FetchLimiter<String>(10, TimeUnit.MINUTES);
    }

    public LiveData<DataRequest<Movie>> loadMovie(final String movieId) {
        return new DataManager<Response, Movie>(taskExecutors) {
            @Override
            protected LiveData<Movie> loadFromDatabase() {
                return movieDatabase.movieDao().getMovie(movieId);
            }

            @Override
            protected LiveData<Response> loadFromNetwork() {
                return null;
            }

            @Override
            protected boolean shouldFetchData(@Nullable Movie data) {
                return false;
            }

            @Override
            protected void saveDataToDatabase(Movie data) {

            }

            @Override
            protected Movie processResponse(Response response) {
                return null;
            }

            @Override
            protected void clearPreviousData() {

            }
        }.toLiveData();
    }


    public LiveData<DataRequest<List<Movie>>> loadMovieList(final String searchString, final boolean forceRefresh) {
        return new DataManager<DataRequest<Response>, List<Movie>>(taskExecutors) {
            @Override
            protected LiveData<List<Movie>> loadFromDatabase() {
                return movieDatabase.movieDao().getMovieList();
            }

            @SuppressLint("CheckResult")
            @Override
            protected LiveData<DataRequest<Response>> loadFromNetwork() {
                MutableLiveData<DataRequest<Response>> responseFromNetwork = new MutableLiveData<>();
                movieListApi.searchMovie(ApiUrls.getApiToken(), searchString, "movie")
                        .subscribeOn(Schedulers.from(taskExecutors.getNetworkOperationThread()))
                        .observeOn(Schedulers.from(taskExecutors.getMainThread())).subscribe(new NetworkOperationObserver(responseFromNetwork, this));
                return responseFromNetwork;
            }

            @Override
            protected List<Movie> processResponse(DataRequest<Response> dataRequest) {
                if (dataRequest.getData() == null)
                    return null;
                return dataRequest.getData().getMovies();
            }

            @Override
            protected boolean shouldFetchData(@Nullable List<Movie> data) {
                return data == null || data.isEmpty() || MovieRepository.this.movieListFetchLimiter.shouldFetch(searchString) || forceRefresh;
            }

            @Override
            protected void saveDataToDatabase(List<Movie> data) {
                movieDatabase.movieDao().insert(data);
            }

            @Override
            protected void clearPreviousData() {
                movieDatabase.movieDao().removeAllMovies();
            }
        }.toLiveData();
    }
}
