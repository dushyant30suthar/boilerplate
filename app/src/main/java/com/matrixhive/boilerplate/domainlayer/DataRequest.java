package com.matrixhive.boilerplate.domainlayer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/*
 * Wrapper class for Any type of request and response from any where like database or network.*/
public class DataRequest<T> {
    private State currentState;
    private T data;
    private String message;


    private DataRequest(@NonNull State currentStatus, @Nullable T data, @Nullable String message) {
        this.currentState = currentStatus;
        this.data = data ;
        this.message=message;
    }

    public State getCurrentState() {
        return currentState;
    }

    public T getData() {
        return data;
    }

    public static <T> DataRequest<T> succeed(@NonNull T data) {
        return new DataRequest<>(State.SUCCEED, data,null);
    }

    public static <T> DataRequest<T> failed(String msg, @Nullable T data) {
        return new DataRequest<>(State.FAILED, data, msg);
    }

    public static <T> DataRequest<T> loading(@Nullable T data) {
        return new DataRequest<>(State.LOADING, data, null);
    }


    public enum State {
        LOADING,
        FAILED,
        SUCCEED
    }
}
