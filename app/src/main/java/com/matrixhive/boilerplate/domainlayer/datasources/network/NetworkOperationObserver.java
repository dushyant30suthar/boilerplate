package com.matrixhive.boilerplate.domainlayer.datasources.network;

import com.matrixhive.boilerplate.domainlayer.DataRequest;
import com.matrixhive.boilerplate.domainlayer.managers.DataManager;
import com.matrixhive.boilerplate.domainlayer.models.Response;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class NetworkOperationObserver implements Observer<Response> {
    MutableLiveData<DataRequest<Response>> responseFromNetwork;
    DataManager dataManager;

    public NetworkOperationObserver(MutableLiveData<DataRequest<Response>> responseFromNetwork, DataManager dataManager) {
        this.responseFromNetwork=responseFromNetwork;
        this.dataManager=dataManager;
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Response response) {
        responseFromNetwork.setValue(DataRequest.succeed(response));
    }

    @Override
    public void onError(Throwable e) {
        this.responseFromNetwork.setValue(DataRequest.failed(e.getLocalizedMessage(), null));
        dataManager.onFetchFailed(e);
    }

    @Override
    public void onComplete() {

    }
}
