package com.matrixhive.boilerplate.domainlayer.managers;


import android.os.SystemClock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import kotlin.jvm.Synchronized;

/*
 * Stores what request we executed and when.*/
public class FetchLimiter<KEY> {
    private Map<KEY, Long> timestamps;
    private Long timeOut;

    public FetchLimiter(int timeOut, TimeUnit timeUnit) {

        timestamps = new HashMap<KEY, Long>();
        this.timeOut = timeUnit.toMillis(timeOut);
    }

    /*
     * Decides whether we should execute network request or not
     * on basis of time.*/
    @Synchronized
    public boolean shouldFetch(KEY key) {
        Long lastFetched = timestamps.get(key);
        Long now = now();
        if (lastFetched == null) {
            timestamps.put(key, now);
            return true;
        }
        if (now - lastFetched > timeOut) {
            timestamps.put(key, now);
            return true;
        }
        return false;
    }

    private Long now() {
        return SystemClock.uptimeMillis();
    }

    @Synchronized
    void reset(KEY key) {
        timestamps.remove(key);
    }
}
