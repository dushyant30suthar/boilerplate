
package com.matrixhive.boilerplate.domainlayer.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("Search")
    private List<Movie> movies = null;
    @SerializedName("totalResults")
    private String totalResults;
    @SerializedName("Response")
    private String response;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
