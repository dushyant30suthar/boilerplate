package com.matrixhive.boilerplate.domainlayer.datasources.network;

import com.matrixhive.boilerplate.domainlayer.models.Response;
import com.matrixhive.boilerplate.framework.BoilerplateApplication;
import com.matrixhive.boilerplate.framework.helpers.NetworkUtil;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

@Module
public class NetworkModule {

    BoilerplateApplication boilerplateApplication;

    public NetworkModule(BoilerplateApplication boilerplateApplication) {
        this.boilerplateApplication = boilerplateApplication;
    }

    @Singleton
    @Provides
    public OkHttpClient getHttpClient() {
        /*
         * Do caching magic here.*/
        final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = chain ->
        {
            okhttp3.Response originalResponse = chain.proceed(chain.request());
            if (NetworkUtil.isNetworkConnected(boilerplateApplication)) {
                int maxAge = 60; // read from cache for 1 minute
                return originalResponse.newBuilder().header("Cache-Control", "public, max-age=" + maxAge).build();
            } else {
                int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                return originalResponse.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale).build();
            }
        };

        File httpCacheDirectory = new File(boilerplateApplication.getCacheDir(), "responses");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(httpCacheDirectory, cacheSize);
        return new OkHttpClient.Builder().addInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR).cache(cache).build();
    }

    @Provides
    @Singleton
    @Inject
    public Retrofit getRetrofitClient(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(ApiUrls.getBaseUrl()).client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiUrls.getBaseUrl())
                .build();
    }

    @Provides
    @Singleton
    @Inject
    public MovieListApi getMovieListApi(Retrofit retrofit) {
        return retrofit.create(MovieListApi.class);
    }

    public interface MovieListApi {
        //http://www.omdbapi.com/?s=inter&apikey=13b629a1&type=movie
        @GET("/")
        Observable<Response> searchMovie(@Query("apikey") String apikey, @Query("s") String searchString, @Query("type") String type);
    }
}
