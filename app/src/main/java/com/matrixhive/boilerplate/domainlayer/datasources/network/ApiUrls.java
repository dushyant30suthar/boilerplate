package com.matrixhive.boilerplate.domainlayer.datasources.network;

import okhttp3.HttpUrl;

public class ApiUrls {

    private static HttpUrl.Builder httpUrl;
    private static String AUTHORITY = "www.omdbapi.com";
    private static String PROTOCOL_SCHEME = "http";
    private static String API_TOKEN = "13b629a1";

    private static HttpUrl.Builder getHttpUrlBuilder() {
        if (httpUrl == null) {
            httpUrl = new HttpUrl.Builder();
        }
        else
        {
            httpUrl.build().newBuilder();
        }
        return httpUrl;
    }

    private static String getAuthority() {
        return AUTHORITY;
    }

    public static HttpUrl getBaseUrl() {
        return getHttpUrlBuilder().scheme(getProtocolScheme()).host(getAuthority()).build();
    }

    private static String getProtocolScheme() {
        return PROTOCOL_SCHEME;
    }

    public static String getApiToken() {
        return API_TOKEN;
    }
}
