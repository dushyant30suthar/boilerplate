package com.matrixhive.boilerplate.domainlayer.managers;

import com.matrixhive.boilerplate.domainlayer.DataRequest;
import com.matrixhive.boilerplate.framework.TaskExecutors;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

/*
 * Hero of our application. It performs whole flow of data.
 * What to do after something.*/
public abstract class DataManager<RequestType, ResultType> {

    private MediatorLiveData<DataRequest<ResultType>> result;
    private TaskExecutors taskExecutors;

    protected DataManager(TaskExecutors taskExecutors) {
        this.taskExecutors = taskExecutors;
        result = new MediatorLiveData<>();
        result.setValue(DataRequest.loading(null));
        LiveData<ResultType> sourceDatabase = loadFromDatabase();
        result.addSource(sourceDatabase, data -> {
            result.removeSource(sourceDatabase);
            if (shouldFetchData(data)) {
                fetchFromNetwork(sourceDatabase);
            } else {
                result.addSource(sourceDatabase, newDataFromDatabase ->
                        setValue(DataRequest.succeed(newDataFromDatabase)));
            }

        });
    }

    /*
     * This method works asynchronously by Room's own implementation.*/
    protected abstract LiveData<ResultType> loadFromDatabase();

    /*
     * This method works asynchronously by RxPatten.*/
    protected abstract LiveData<RequestType> loadFromNetwork();

    protected abstract boolean shouldFetchData(@Nullable ResultType data);

    protected abstract void saveDataToDatabase(ResultType data);

    protected abstract void clearPreviousData();

    public void onFetchFailed(Throwable throwable) {
        setValue(DataRequest.failed(throwable.getLocalizedMessage(), null));
    }

    /*
     * Updates the live data which we are interested in.*/
    private void setValue(DataRequest<ResultType> newValue) {
        if (result.getValue() != newValue) {
            result.setValue(newValue);
        }
    }

    public LiveData<DataRequest<ResultType>> toLiveData() {
        return result;
    }

    protected abstract ResultType processResponse(RequestType response);

    private void fetchFromNetwork(LiveData<ResultType> sourceDatabase) {
        LiveData<RequestType> sourceNetwork = loadFromNetwork();
        result.addSource(sourceDatabase,
                dataFromDatabase ->
                        /*setValue(DataRequest.loading(dataFromDatabase)));*/
                        setValue(DataRequest.loading(null)));
        result.addSource(sourceNetwork,
                dataFromNetwork ->
                {
                    result.removeSource(sourceNetwork);
                    result.removeSource(sourceDatabase);
                    taskExecutors.getDiskOperationThread().execute(() ->
                    {
                        ResultType processedData = processResponse(dataFromNetwork);
                        if (processedData == null) {
                            taskExecutors.getMainThread().execute(() -> setValue(DataRequest.failed("Not Found", null)));
                            return;
                        }
                        clearPreviousData();
                        saveDataToDatabase(processedData);
                        taskExecutors.getMainThread().execute(() ->
                                result.addSource(loadFromDatabase(),
                                        newDataFromDatabase ->
                                                setValue(DataRequest.succeed(newDataFromDatabase))));
                    });

                    /*
                     * Here we can handle various type of responses we can get form network,
                     * whether it can be empty or some error on page or anything like that.*/

                    /*switch (dataFromNetwork.getCurrentState()) {
                        case SUCCEED: {
                            taskExecutors.getDiskOperationThread().execute(() ->
                            {
                                saveDataToDatabase(processResponse(dataFromNetwork));
                                taskExecutors.getMainThread().execute(() ->
                                        result.addSource(loadFromDatabase(),
                                                newDataFromDatabase ->
                                                        setValue(DataRequest.succeed(newDataFromDatabase))));
                            });
                        }
                        break;
                        case EMPTY: {
                            taskExecutors.getMainThread().execute(() -> result.addSource(loadFromDatabase(),newDataFromDatabase -> setValue(DataRequest.succeed(newDataFromDatabase))));
                        }
                        break;
                        case ERROR: {
                            onFetchFailed();
                            result.addSource(sourceDatabase,newDataFromDatabase -> setValue(DataRequest.failed("Error Message",newDataFromDatabase)));

                        }
                        break;
                    }*/
                });
    }
}
