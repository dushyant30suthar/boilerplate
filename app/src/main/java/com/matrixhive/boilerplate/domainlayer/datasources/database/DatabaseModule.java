package com.matrixhive.boilerplate.domainlayer.datasources.database;

import android.content.Context;

import com.matrixhive.boilerplate.domainlayer.models.Movie;

import java.util.List;

import javax.inject.Singleton;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    Context context;

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public MovieDatabase getMovieDatabase() {
        return Room.databaseBuilder(context,
                MovieDatabase.class, "movie_database").build();
    }


    @Dao
    public interface MovieDao {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        long[] insert(List<Movie> movieList);

        @Query("SELECT * from movie")
        LiveData<List<Movie>> getMovieList();

        @Query("SELECT * from movie WHERE imdbID = :id")
        LiveData<Movie> getMovie(String id);

        @Query("DELETE FROM movie")
        void removeAllMovies();

        /*@Query("DELETE FROM ")
        void deleteAll();

        @Query("SELECT * from ORDER BY ASC")
        LIST<> getAllWords();*/
    }

    @Database(entities = {Movie.class}, version = 1, exportSchema = false)
    public static abstract class MovieDatabase extends RoomDatabase {
        public abstract MovieDao movieDao();
    }
}