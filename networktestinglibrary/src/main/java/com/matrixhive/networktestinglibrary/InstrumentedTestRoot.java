package com.matrixhive.networktestinglibrary;

import android.content.Context;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class InstrumentedTestRoot {
    @Test
    public void useAppContext(String packageName) {
        Context appContext = InstrumentationRegistry.getTargetContext();
        //asserting
        Assert.assertEquals(packageName, appContext.getPackageName());
    }
}
