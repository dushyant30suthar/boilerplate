package com.matrixhive.networktestinglibrary;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

public class MockServerDispatcher {

    String requestPath, response;
    int responseCode;

    public MockServerDispatcher() {

    }

    public MockServerDispatcher(String requestPath, int responseCode, String response) {
        this.requestPath = requestPath;
        this.response = response;
        this.responseCode = responseCode;
    }


    /**
     * Return ok response from mock server
     */
    public enum DispatcherType {
        REQUEST, ERROR
    }

    public class RequestDispatcher
            extends
            Dispatcher {


        @Override
        public MockResponse dispatch(
                RecordedRequest request) {
            if (request.getPath().equals(requestPath)) {
                return new MockResponse().addHeader("Content-Type: application/json").setResponseCode(responseCode).setBody(response);
            } else {
                return new MockResponse().setResponseCode(404);
            }
        }
    }

    /**
     * Return error response from mock server
     */
    public class ErrorDispatcher
            extends
            Dispatcher {

        @Override
        public MockResponse dispatch(
                RecordedRequest request) {
            return new MockResponse().setResponseCode(400);
        }
    }
}
