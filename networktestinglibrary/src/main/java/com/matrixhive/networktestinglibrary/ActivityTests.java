package com.matrixhive.networktestinglibrary;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.mockwebserver.MockWebServer;

@RunWith(AndroidJUnit4.class)
public abstract class ActivityTests {

    /*
    * We need to declare a ActivityTest Rule like this.
    * @Rule
    public ActivityTestRule<TestActivity> activityRule = new ActivityTestRule<>(TestActivity.class, false, false);*/

    private MockWebServer webServer;

    @Before
    public void setup() throws
            Exception {

        webServer = new MockWebServer();
        webServer.start(8080);
    }

    @After
    public void tearDown() throws
            Exception {
        webServer.shutdown();
    }

    protected void setDispatcher(
            MockServerDispatcher mockServerDispatcher, MockServerDispatcher.DispatcherType dispatcherType) {
        switch (dispatcherType) {
            case REQUEST:
                webServer.setDispatcher(mockServerDispatcher.new RequestDispatcher());
                break;
            case ERROR:
                webServer.setDispatcher(mockServerDispatcher.new ErrorDispatcher());
                break;
        }
    }

    /*
     * Start your test here but you need to set Dispatchers and responses first.
     * call setDispatcher() to set that*/
    @Test
    public abstract void testHappyCondition() throws
            IOException;

    @Test
    public abstract void testFailedCondition();

}
