package com.matrixhive.networktestinglibrary;

import android.os.Bundle;
import android.os.StrictMode;

import androidx.test.runner.AndroidJUnitRunner;

public class MockRunner
        extends
        AndroidJUnitRunner {

    @Override
    public void onCreate(
            Bundle arguments) {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        super.onCreate(arguments);
    }
}